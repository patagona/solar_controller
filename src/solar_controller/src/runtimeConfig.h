#pragma once

#include <stdint.h>
#include "staticConfig.h"

struct InputConfig
{
    float currentLimit;
};

struct LoadConfig
{
    float enableVoltage;
    float disableVoltage;
};

enum class BatteryChargeMode
{
    cv,
    cc
};

enum class BatteryType
{
    pbSealed,
    liIon,
    liPo
};

struct BatteryConfig
{
    uint8_t numSerialCells;
    float cRate;
    float capacity;
    BatteryType batteryType;
};