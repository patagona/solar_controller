#include <Arduino.h>
#include "staticConfig.h"
#include "runtimeConfig.h"
#include "status.h"

void tick();
void measureInput();
void measureBattery();
void batteryTick();
void loadTick();
bool isInputLimit();
bool isBatteryLimit();
float measureVoltage(uint8_t pin, float voltsPerAdcStep, float *samples);
float measureCurrent(uint8_t pin, float ampsPerAdcStep, float offsetCurrent, float *samples);
float addSampleGetAverage(float *samples, float sample);
int8_t getMpptAction();
void changeInputPwm(int8_t amount);

// Static Config
#define LOAD_NUM 1
#define NUM_SAMPLES 10

const struct StaticInputConfig staticInputConfig = {
    .pwmPin = 3,
    .currentPin = A2,
    .ampsPerAdcStep = 0.001f,
    .offsetCurrent = 50.0f
};

const struct StaticBatteryConfig staticBatteryConfig = {
    .currentPin = A0,
    .ampsPerAdcStep = 0.001f,
    .offsetCurrent = 50.0f,

    .voltagePin = A1,
    .voltsPerAdcStep = 0.001f
};

const ConverterMode converterMode = ConverterMode::pwm;

const int loadPins[LOAD_NUM] = {1};

const int tickRate = 100; // ms

const float mpptPowerThreshold = 0.1; // W

// End Static Config

// Dynamic Config

InputConfig inputConfig = {
    .currentLimit = 30 //A
};

BatteryConfig batteryConfig = {
    .numSerialCells = 6,
    .cRate = 0.4f,    // C
    .capacity = 7.2f, // Ah
    .batteryType = BatteryType::pbSealed
};

LoadConfig defaultLoad = {
    .enableVoltage = 13.0f,
    .disableVoltage = 11.5f
};

LoadConfig loadConfigs[LOAD_NUM] = {defaultLoad};

// End Dynamic Config

InputStatus inputStatus = {0};
BatteryStatus batteryStatus = {0};
LoadStatus loadStatus[LOAD_NUM] = {0};

float batteryVoltageSamples[NUM_SAMPLES] = {0};
float batteryCurrentSamples[NUM_SAMPLES] = {0};

float inputCurrentSamples[NUM_SAMPLES] = {0};

uint8_t currentInputPwm = 0;

void setup()
{
    Serial.begin(115200)
    pinMode(staticInputConfig.currentPin, INPUT);
    pinMode(staticBatteryConfig.voltagePin, INPUT);
    pinMode(staticBatteryConfig.currentPin, INPUT);

    pinMode(staticInputConfig.pwmPin, OUTPUT);

    for(int i = 0; i < LOAD_NUM; i++)
    {
        pinMode(loadPins[i], OUTPUT);
    }
}

unsigned long lastTickMillis = 0;

void loop()
{
    unsigned long nowMillis = millis();
    if (nowMillis - lastTickMillis > tickRate)
    {
        tick();
        lastTickMillis = nowMillis;
    }
}

void tick()
{
    measureInput();
    measureBattery();
    batteryTick();
    loadTick();
}

void measureBattery()
{
    batteryStatus.voltage = measureVoltage(staticBatteryConfig.voltagePin, staticBatteryConfig.voltsPerAdcStep, batteryVoltageSamples);
    batteryStatus.current = measureCurrent(staticBatteryConfig.currentPin, staticBatteryConfig.ampsPerAdcStep, staticBatteryConfig.offsetCurrent, batteryCurrentSamples);
}

void measureInput()
{
    inputStatus.current = measureCurrent(staticInputConfig.currentPin, staticInputConfig.ampsPerAdcStep, staticInputConfig.offsetCurrent, inputCurrentSamples);
}

float measureVoltage(uint8_t pin, float voltsPerAdcStep, float *samples)
{
    int adcValue = analogRead(pin);
    float volts = adcValue * voltsPerAdcStep;
    return addSampleGetAverage(samples, volts);
}

float measureCurrent(uint8_t pin, float ampsPerAdcStep, float offsetCurrent, float *samples)
{
    int adcValue = analogRead(pin);
    float amps = (adcValue * ampsPerAdcStep) - offsetCurrent;

    return addSampleGetAverage(samples, amps);
}

float addSampleGetAverage(float *samples, float sample)
{
    float sampleSum = 0;

    for(size_t i = 0; i < NUM_SAMPLES; i++)
    {
        float nextSample;
        if(i == NUM_SAMPLES - 2)
        {
            nextSample = sample;
        }
        else
        {
            nextSample = samples[i+1];
        }

        samples[i] = nextSample;
        sampleSum += nextSample;
    }

    return sampleSum / NUM_SAMPLES;
}

void batteryTick()
{
    bool limitHit = false;

    limitHit |= isInputLimit();
    limitHit |= isBatteryLimit();

    int8_t changeAmount = 0;

    if (limitHit)
    {
        changeAmount = -1;
    }
    else
    {
        switch (converterMode)
        {
            case ConverterMode::mppt:
                changeAmount = getMpptAction();
                break;
            case ConverterMode::pwm:
                changeAmount = 1;
                break;
            default:
                break;
        }
    }
    changeInputPwm(changeAmount);
}

int8_t getMpptAction()
{
    static int8_t mpptAction = 1;
    static float lastPower = 0;

    float power = batteryStatus.voltage * batteryStatus.current;

    if(abs(power - lastPower) > mpptPowerThreshold){
        if(power < lastPower){
            mpptAction = -mpptAction;
        }

        lastPower = power;
    }
    return mpptAction;
}

void changeInputPwm(int8_t amount)
{
    int16_t newValue = currentInputPwm + (int16_t)amount;
    if(newValue > 255)
        newValue = 255;
    if(newValue < 0)
        newValue = 0;

    analogWrite(staticInputConfig.pwmPin, currentInputPwm);
}

bool isInputLimit()
{
    bool isLimit = false;
    isLimit |= inputConfig.currentLimit > inputConfig.currentLimit;

    if (converterMode == ConverterMode::mppt)
    {
        //TODO: hit limit if mppt point hit
    }

    return isLimit;
}

bool isBatteryLimit()
{
    bool isLimit = false;

    float maxBatteryCurrent = batteryConfig.capacity * batteryConfig.cRate;
    float maxBatteryVoltage = 0.0f;

    switch (batteryConfig.batteryType)
    {
    case BatteryType::pbSealed:
        maxBatteryVoltage = batteryConfig.numSerialCells * 2.3f;
        break;
    case BatteryType::liPo:
        maxBatteryVoltage = batteryConfig.numSerialCells * 4.2f;
        break;
    case BatteryType::liIon:
        maxBatteryVoltage = batteryConfig.numSerialCells * 4.1f;
    default:
        break;
    }

    isLimit |= batteryStatus.current > maxBatteryCurrent;
    isLimit |= batteryStatus.voltage > maxBatteryVoltage;

    return isLimit;
}

void loadTick()
{
    for (int i; i < LOAD_NUM; i++)
    {
        uint8_t loadPin = loadPins[i];
        LoadConfig loadConfig = loadConfigs[i];

        if (loadStatus[i].active)
        {
            if (batteryStatus.voltage < loadConfig.disableVoltage)
            {
                digitalWrite(loadPin, false);
                loadStatus[i].active = false;
            }
        }
        else
        {
            if (batteryStatus.voltage > loadConfig.enableVoltage)
            {
                digitalWrite(loadPin, true);
                loadStatus[i].active = true;
            }
        }
    }
}