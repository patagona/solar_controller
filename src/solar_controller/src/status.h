#pragma once

struct InputStatus
{
    float current;
};

struct BatteryStatus
{
    float voltage;
    float current; //positive = charge, negative = discharge
};

struct LoadStatus
{
    bool active;
};