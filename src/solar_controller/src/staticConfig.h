#pragma once

#include <Arduino.h>
#include <stdint.h>

enum class ConverterMode
{
    pwm,
    mppt
};

struct StaticInputConfig
{
    uint8_t pwmPin;

    uint8_t currentPin;
    float ampsPerAdcStep;
    float offsetCurrent;
};

struct StaticBatteryConfig
{
    uint8_t currentPin;
    float ampsPerAdcStep;
    float offsetCurrent;

    uint8_t voltagePin;
    float voltsPerAdcStep;
};