# another microcontroller-based Solar charge controller
## TODO:
- multi-stage-battery charging

## Planned features:
- charge voltage and current monitoring
- battery voltage and current limit
- mppt (if used as a switching converter)
- multi-stage-battery charging
